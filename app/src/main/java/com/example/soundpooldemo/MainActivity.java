package com.example.soundpooldemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final HashMap<Integer, Integer> soundIds = new HashMap<>();
    private AssetManager assetManager;
    private SoundPool soundPool;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.bt_main_play1).setOnClickListener(this);
        findViewById(R.id.bt_main_play2).setOnClickListener(this);
        findViewById(R.id.bt_main_play3).setOnClickListener(this);
        findViewById(R.id.bt_main_play4).setOnClickListener(this);
        findViewById(R.id.bt_main_play5).setOnClickListener(this);
        findViewById(R.id.bt_main_release).setOnClickListener(this);

        assetManager = getAssets();
        initSoundPool();
    }

    private void initSoundPool() {
        if (Build.VERSION.SDK_INT >= 21) {
            //argetSDK版本要设置大于等于21哦！而且如果minSDK版本小于21 否则会报错
            SoundPool.Builder soundPoolBuilder = new SoundPool.Builder();
            soundPoolBuilder.setMaxStreams(5);
            AudioAttributes.Builder audioAttributesBuilder = new AudioAttributes.Builder();
            audioAttributesBuilder.setLegacyStreamType(AudioManager.STREAM_SYSTEM);//同下
            soundPoolBuilder.setAudioAttributes(audioAttributesBuilder.build());
            soundPool = soundPoolBuilder.build();
        } else {
            /**
             * maxStreams 指定支持多少个声音，SoundPool对象中允许同时存在的最大流的数量
             * streamType 指定声音类型，流类型可以分为STREAM_VOICE_CALL, STREAM_SYSTEM, STREAM_RING,STREAM_MUSIC 和 STREAM_ALARM四种类型。在AudioManager中定义。
             * srcQuality 指定声音品质（采样率变换质量），一般直接设置为0！
             */
            soundPool = new SoundPool(5, AudioManager.STREAM_SYSTEM, 5);
        }
        // priority 指定播放声音的优先级，数值越高，优先级越大。
        soundIds.put(1, soundPool.load(this, R.raw.duang, 1));
        try {
            soundIds.put(2, soundPool.load(assetManager.openFd("biaobiao.mp3"), 1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        soundIds.put(3, soundPool.load(this, R.raw.duang, 1));
        soundIds.put(4, soundPool.load(this, R.raw.duang, 1));
        soundIds.put(5, soundPool.load(this, R.raw.duang, 1));

        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                Toast.makeText(MainActivity.this, "音频已准备好了!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_main_play1:
                /*
                    soundID：Load()返回的声音ID号
                    leftVolume：左声道音量设置
                    rightVolume：右声道音量设置
                    priority：指定播放声音的优先级，数值越高，优先级越大。
                    loop：指定是否循环：-1表示无限循环，0表示不循环，其他值表示要重复播放的次数
                    rate：指定播放速率：1.0的播放率可以使声音按照其原始频率，而2.0的播放速率，可以使声音按照其 原始频率的两倍播放。
                    如果为0.5的播放率，则播放速率是原始频率的一半。播放速率的取值范围是0.5至2.0。
                 */
                soundPool.play(soundIds.get(1), 1, 1, 0, 0, 0);
                break;
            case R.id.bt_main_play2:
                soundPool.play(soundIds.get(2), 1, 1, 0, 0, 0);
                break;
            case R.id.bt_main_play3:
                soundPool.play(soundIds.get(3), 1, 1, 0, 0, 0);
                break;
            case R.id.bt_main_play4:
                soundPool.play(soundIds.get(4), 1, 1, 0, 0, 0);
                break;
            case R.id.bt_main_play5:
                soundPool.play(soundIds.get(5), 1, 1, 0, 0, 0);
                break;
            case R.id.bt_main_release:
                //可以调用release()方法释放所有SoundPool对象占据的内存和资源，当然也可以根据声音 ID来释放！
                soundPool.release();
                break;
        }
    }
}